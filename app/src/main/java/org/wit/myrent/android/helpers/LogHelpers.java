package org.wit.myrent.android.helpers;

/**
 * Created by ictskills on 23/09/16.
 */


import android.util.Log;



public class LogHelpers
{
    public static void info(Object parent, String message)
    {
        Log.i(parent.getClass().getSimpleName(), message);
    }


}